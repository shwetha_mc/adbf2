CC=gcc
EVPATH_ROOT=/home/shwetha_mc
DFG_ROOT=/home/shwetha_mc/dfg/
CTRL_ROOT=/home/shwetha_mc/adbf2/control_test_monpp
HDRFILES=include/init.h include/setup.h
LDFLAGS=-lm -lrt -lpthread 
CFLAGS=-std=c99
HEADERS= $(HDRFILES)
SOURCEDIR=src/
BINDIR=bin/
LIBRARIES=-L/home/shwetha_mc/lib -levpath -lcercs_env -latl -lffs -ldill -lpthread
#LIBS= -levpath -lcercs_env -latl -lffs -ldill -lpthread
HEADERIMP=src/init.c src/setup.c
BNODE=bnode
CNODE=covariance_rcu
INCS=-I. -I$(EVPATH_ROOT)/include -I$(DFG_ROOT) -I$(CTRL_ROOT)/include -I$(EVPATH_ROOT)/adbf2/evpath40/build_area/evpath/source -I$(EVPATH_ROOT)/adbf2/evpath40/build_area/evpath/build


all: $(CNODE) $(BNODE)

$(BNODE): $(BNODE).o $(CTRL_ROOT)/build/dfg_functions.o
	$(CC) -o $(BINDIR)$@ $^ $(HEADERS) $(HEADERIMP) $(INCS) $(LDFLAGS) $(LIBRARIES)

$(CNODE): $(CNODE).o $(CTRL_ROOT)/build/dfg_functions.o
	$(CC) -o $(BINDIR)$@ $^ $(HEADERS) $(HEADERIMP) $(INCS) $(LDFLAGS) $(LIBRARIES)


$(BNODE).o:
	$(CC) -c $(SOURCEDIR)$(BNODE).c $(HEADERS) $(HEADERIMP) $(INCS) $(LDFLAGS) $(LIBRARIES)

$(CNODE).o:
	$(CC) -c $(SOURCEDIR)$(CNODE).c $(HEADERS) $(HEADERIMP) $(INCS) $(LDFLAGS) $(LIBRARIES)

.PHONY: clean

clean:
	rm -f bin/bnode bin/covariance bin/setup bin/init *.o *~
