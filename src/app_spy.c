#include <sys/ucontext.h>
#include <unistd.h>
#include <signal.h>
#include <stdio.h>
#include <malloc.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/mman.h>
#include "evpath.h"
#include "ev_dfg.h"
#define PROT_SIZE 4

struct timeval mon_sent;

static char mastercontact[2048];

typedef struct _mon_app_data {
        int rangebins;
        int oversample;
} mon_app_data, *mon_app_ptr;

CManager cm;
char nodename[] = "cnode";
EVclient test_client;
EVsource a_source;

static FMField mon_app_data_field_list[] =
{
        {"rangebins","integer",sizeof(int), FMOffset(mon_app_ptr, rangebins)},
         {"oversample","integer",sizeof(int), FMOffset(mon_app_ptr, oversample)},
         {NULL, NULL, 0, 0}
};

static FMStructDescRec mon_app_spy_format_list[] =
{
        {"mon_app_data",mon_app_data_field_list,sizeof(mon_app_data),NULL},
        {NULL, NULL}
};



static int proti=0;

static int prot_arr[]={PROT_NONE,PROT_READ,PROT_WRITE,PROT_EXEC};
static char datatype;
static char *varname;

int setvarname(char *name)
{
	varname = name;
	return 1;
}


int setdtype(char dtype)
{
    datatype=dtype;
    return 1;
}

static void
handler(int sig, siginfo_t *si, void *unused)
{
    printf("%c\n",datatype );
           printf("Got SIGSEGV at address: 0x%lx\n", (long) si->si_addr);
	//char nodenamea[]="cnode";
	char sourcenamea[]="src_a_spy_cnode";
	printf("Mastercontact %s nodename %s App_spy\n",mastercontact, nodename);
    switch(datatype)
    {
        case 'i': proti=(proti+1)%PROT_SIZE; 
                  mprotect(si->si_addr, sizeof(int),4);
		  int *rb = (int*)si->si_addr;
		  printf("%d rangebins \n",*rb);
		  //cm=CManager_create();
		  //EVclient test_client;
		  //EVclient_sources source_capabilities;
	          //dfg = EVdfg_create(cm);
		  //EVsource source;
//		  sourcenamea="src_a_spy_cnode";
		  //nodenamea="app_spy_cnode";
 		  /*source = EVcreate_submit_handle(cm,DFG_SOURCE,mon_app_spy_format_list);         
		  source_capabilities=EVclient_register_source(sourcenamea, source);
		  test_client = EVclient_assoc(cm,nodename,mastercontact,source_capabilities,NULL);
		  EVclient_ready_wait(test_client);*/
		  mon_app_data dat;
		  dat.rangebins=*rb;
		  dat.oversample=8;

		  if(EVclient_source_active(a_source)) {
			gettimeofday(&mon_sent,NULL);
			printf("App spy sending data at %ld usec\n",(mon_sent.tv_usec + (mon_sent.tv_sec * 1000000)));
			EVsubmit(a_source,&dat,NULL);
			}
	//	  EVfree_source(a_source);
		  //EVclient_ready_for_shutdown(test_client);
		  EVclient_wait_for_shutdown(test_client);
//		  printf("App spy ends, %d %d\n",__LINE__,*rb);
		  mprotect(si->si_addr, sizeof(int),2);

		  break;
        default:   proti=(proti+1)%PROT_SIZE; 
                  mprotect(si->si_addr, sizeof(double),prot_arr[proti]);
                  break;

    }
return;
}


void app_setup(char *name,char dtype)
{
    struct sigaction sa;
    sa.sa_flags = SA_SIGINFO;
    sigemptyset(&sa.sa_mask);
    sa.sa_sigaction = handler;
    setdtype(dtype);
    setvarname(name);
    sigaction(SIGSEGV, &sa, NULL);
}

