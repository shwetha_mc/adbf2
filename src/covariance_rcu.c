#include<stdio.h>
#include<stdlib.h>
#include<complex.h>
#include<string.h>
#include<arpa/inet.h>
#include<sys/time.h>
//#include <time.h>
#include "../include/setup.h"
#include "evpath.h"
#include "atl.h"
#define NX 16
#define NY 16
#define SIGRANGEBIN 50
#define NXY NX*NY
#include "app_spy.c"
//#include "ctrl_init.h"


#define NUMEVENTS 5

#define FOREACH_EVENT(ctrl_EVENT_ID) \
        ctrl_EVENT_ID(ctrl_START_APP) \
        ctrl_EVENT_ID(ctrl_STOP_APP) \
        ctrl_EVENT_ID(ctrl_DESCOPE) \
        ctrl_EVENT_ID(ctrl_OVERSAMPLE) \
        ctrl_EVENT_ID(ctrl_RECONFIGURE) \

#define GENERATE_ENUM(ENUM) ENUM,
#define GENERATE_STRING(STRING) #STRING,

typedef enum _ctrl_EVENT_ID {
        FOREACH_EVENT(GENERATE_ENUM)
} ctrl_EVENT_ID;



static const char *ctrl_EVENT_ID_ARR[] = {
        FOREACH_EVENT(GENERATE_STRING)
};





struct timeval sent,received;
EVclient test_client;
EVsource src_dum;

int *rangebins;
//int* *rangebinsptr=&rangebins;

typedef struct {
 double real;
 double imag;
}comp;

/*Data descriptor for App data*/
typedef struct _inverse {
    comp inv_data[NXY][NXY];
    comp raw_data[NX][NY];
} inverse, *inverse_ptr;
static FMField compfield_list[] =
{
    {"real", "float", sizeof(float), FMOffset(comp*, real)},
   {"imag", "float", sizeof(float), FMOffset(comp*, imag)},
    {NULL, NULL, 0, 0}
};

static FMField inverse_field_list[] =
{
  {"inv_data", "comp[256][256]", sizeof(comp), FMOffset(inverse*, inv_data)},
{"raw_data", "comp[16][16]", sizeof(comp), FMOffset(inverse*, raw_data)},
    {NULL, NULL, 0, 0}
};


static FMStructDescRec inverse_format_list[] =
{
 	{"inverse", inverse_field_list, sizeof(inverse), NULL},
   	{"comp", compfield_list, sizeof(comp), NULL},
    {NULL, NULL}
};

/*Data descriptor for ctrl data*/
typedef struct _control_msg {
        int ctrl_ev;
        int payload;
} ctrl_msg, *ctrl_msg_ptr;


static FMField ctrl_msg_rec_field_list[] =
{
    {"ctrl_ev", "integer", sizeof(int), FMOffset(ctrl_msg_ptr, ctrl_ev)},
    {"payload", "integer", sizeof(int), FMOffset(ctrl_msg_ptr, payload)},
    {NULL, NULL, 0, 0}
};

static FMStructDescRec ctrl_msg_rec_format_list[] =
{
    {"ctrl_msg", ctrl_msg_rec_field_list, sizeof(ctrl_msg), NULL},
    {NULL, NULL}
};

/* control functions follow*/

int descoper(double factor)
{
         printf("%d\n",__LINE__);

        int ret=0;
        if(factor)
        {
	printf("%d\n",__LINE__);
	app_setup("rangebins",'i');
        //mprotect(rangebins, sizeof(int),4);
        *rangebins = *rangebins / factor;
        printf("Rangebins descoped : new value = %d\n",*rangebins);
        //mprotect(rangebins,sizeof(int),2);
        ret=1;
        }
        //ret=0;
        return ret;
}
/*control functions end*/
 
static int ctrl_handler(CManager cm, void *vevent, void *client_data, attr_list attrs)
{
	int temp;
	 printf("%d\n",__LINE__);

	
	ctrl_msg_ptr event = (ctrl_msg_ptr) vevent;
	temp = *rangebins / event->payload;
	//descoper(event->payload);
	EVsubmit(src_dum,event,NULL);
	printf("%d\n",__LINE__);
	EVfree_source(src_dum);
//	descoper(event->payload);
	return 1;
		
}




/*static int ponghandler(CManager cm, void *vevent, void *client_data, attr_list attrs)
{
	gettimeofday(&received,NULL);
//	inverse_ptr event = (inverse_ptr) vevent;
	mon_app_ptr event = (mon_app_ptr) vevent;
	printf("Mon node ponged back at %ld usec \n ",(received.tv_usec + (received.tv_sec * 1000000)));
	//printf("Cnode ponged back at %ld usec\n",(received.tv_usec + (received.tv_sec * 1000000)));
	EVclient_shutdown(test_client,0);

}
*/

double complex mul_comp(double complex a, double complex b)
{
	return(((creal(a)*creal(b))-(cimag(a)*cimag(b)))+I*((creal(a)*cimag(b))+(cimag(a)*creal(b))));
}
double complex div_comp(double complex a, double complex b)
{
	 return((((creal(a)*creal(b))-(cimag(a)*cimag(b)))/((creal(a)*creal(a))+(cimag(b)*cimag(b))))+I*(((creal(a)*cimag(b))+(cimag(a)*creal(b)))/((creal(a)*creal(a))+(cimag(b)*cimag(b)))));
}

/* control functions follow

int descoper(double factor)
{
	 printf("%d\n",__LINE__);

	int ret=0;
 	if(factor)
	{
	mprotect(rangebins, sizeof(int),PROT_EXEC);
	*rangebins = *rangebins / factor;
	printf("Rangebins descoped : new value = %d\n",*rangebins);
	mprotect(rangebins,sizeof(int),prot_arr[proti]);
	ret=1;
	}
	//ret=0;
	return ret;

}

control functions end*/

void vectorize(double complex *jnv, double complex jn[NX][NY])
{

	int i,j,k=0;
	for(i=0;i<NX;++i)
	{
	  for(j=0;j<NY;++j)
	  {
		//if(k<NXY)
		//{
		*(jnv+k)=jn[j][i];
//(round(creal(jn[i][j])*10000)/10000)+I*((round(cimag(jn[i][j])*10000)/10000));
		++k;
	//	}
		
	  }
	}
}

void calc_inverse(double complex *Rinv)
{
	//int nxy=2;
	int nxy=NX*NY;
        int i,j,k,temp;
        double complex iden[nxy][nxy];
        double complex arr[nxy][2*nxy];
        double complex r,swaptemp,factor;
        for(i=0;i<nxy;i++)
        {
                for(j=0;j<nxy;++j)
                arr[i][j]=*(Rinv+(i*nxy)+j);
        }
	
      /*Identity matrix*/
        for(i=0;i<nxy;++i)
        {
                for(j=0;j<nxy;++j)
                        iden[i][j]=(i==j)?(1):0;
        }
        /*augmenting base matrix with identity*/
        for(i=0;i<nxy;++i)
        {
                for(j=0;j<nxy;++j)
                {
                        arr[i][j+nxy]=iden[i][j];
                }
        }
	
        /*Gaussian decomposition*/
	for(k=0;k<nxy;++k)
        {
        factor=((arr[k][k]));
        for(i=0;i<2*nxy;++i)
        {
                arr[k][i]=(arr[k][i]/factor);
        }
        for(i=0;i<nxy;++i)
        {
                if(i!=k)
                {
                factor=((arr[i][k]));
                for(j=0;j<2*nxy;++j)
                {
                        arr[i][j]=arr[i][j]-(arr[k][j]*factor);

                }
                }
        }
        }


//	printf("\nends\n");
	 /*assignment*/
        for(i=0;i<nxy;++i)
        {
                for(j=0;j<nxy;++j)
                {
                        *(Rinv+(i*nxy+j))=arr[i][j+nxy];
                }
        }
	
	/* for(i=0;i<nxy;++i)
        {
                for(j=0;j<nxy;++j)
                {
		printf("%f+i%f\t",creal(*(Rinv+(i*nxy+j))),cimag(*(Rinv+(i*nxy+j))));
		}
		printf("\nline\n");
	}*/
		
	
	
}

void calc_covariance(double complex *Rinv, double complex *raw_data)
{
	int m,i,j,k=0,nxy=NX*NY;
	double complex val=0;
	double complex jam[NX][NY],sig[NX][NY],jn[NX][NY],jncopy[NX][NY],jnv[nxy],R[nxy][nxy];
	int p,q;
	double complex inter[nxy][nxy];
	for(i=0;i<NX;++i)
	{
		for(j=0;j<NY;++j)
		{
			sig[i][j]=0;
                        jam[i][j]=0;
                        jn[i][j]=0;
			*(raw_data+(i*NY)+j)=0;


		}
	}
	for(i=0;i<nxy;++i)
	{
		for(j=0;j<nxy;++j)
		{
			*(Rinv+(i*nxy)+j)=0;
			R[i][j]=0;
		}
	}
	get_jam_signal(&jam[0][0]);
	get_tgt_signal(&sig[0][0]);
	//get_jn(&jn,&jam);	
	
/*	for(i=0;i<NX;++i)
	{
		for(j=0;j<NY;++j)
		{
		jn[i][j]=jam[i][j];
		}
	}*/
	
	if(!(*rangebins))
	{
	FILE *rngbin;
	rngbin=fopen("../rangebins.txt","r");
	fscanf(rngbin,"%d \n",rangebins);
	fclose(rngbin);
	}
	printf("Rangebins set : %d\n", rangebins);
	

	 mprotect(rangebins, sizeof(int),prot_arr[proti]);
	printf("return %d\n",prot_arr[proti]);
	for(m=0;m<(*rangebins);++m)
	{
	
//	int w,v=0;
         get_jn(&jn[0][0],&jam[0][0]);
	 if((m+1)==SIGRANGEBIN)
	 {
		for(i=0;i<NX;++i)
		{
			for(j=0;j<NY;++j)
			{
			jn[i][j]+=sig[i][j];
			*(raw_data+(i*NY+j))=jn[i][j];
			}
		}
	 }
		/*	for(i=0;i<NX;++i)
	{
	for(j=0;j<NY;++j)
	{
		jncopy[i][j]=jn[j][i];
	}
	}*/
	 vectorize(&jnv[0],jn);
	/*printf("jn\n");
	for(i=0;i<NX;++i)
	{
		for(j=0;j<NY;++j)
		printf("%f+i%f\t",creal(jn[i][j]),cimag(jn[i][j]));
		printf("\nline\n");
	}*/
	 
	for(p=0;p<nxy;++p)
	{
		for(q=0;q<nxy;++q)
		inter[p][q]=0;
	}

         for(i=0;i<nxy;++i)
	 {
		for(j=0;j<nxy;++j)
		{
		inter[i][j]=jnv[i]*(conj(jnv[j]));
		}
	}
	
	 for(p=0;p<nxy;++p)
	 {
		for(q=0;q<nxy;++q)
		{
		R[p][q]+=inter[p][q];
		}
	 }
/*	for(i=0;i<NXY;++i)
        {
                for(j=0;j<NXY;++j)
                {
                printf("%f+i%f\t",creal(R[i][j]),cimag(R[i][j]));
                }
                printf("\nline\n");
        }

*/	
	}
	for(i=0;i<nxy;++i)
	{
		for(j=0;j<nxy;++j)
		{
		*(Rinv+(i*nxy)+j)=R[i][j];
		}
	}
/*	 for(i=0;i<NXY;++i)
        {
                for(j=0;j<NXY;++j)
		{
		printf("%f+i%f\t",creal(R[i][j]),cimag(R[i][j]));
		}
		printf("\nline\n");
	}*/
	calc_inverse(Rinv);
       
/*	for(i=0;i<NXY;++i)
        {
                for(j=0;j<NXY;++j)
                {
		//	 printf("%f+i%f\t",creal(R[i][j]),cimag(R[i][j]));

               printf("%f+i%f\t",creal(*(Rinv+(i*nxy+j))),cimag(*(Rinv+(i*nxy+j))));
                }
                printf("\nline\n");
        }
*/

}





int main(int argc, char **argv)
{
  /*Rangebins to be page-aligned in order to be trapped by mprotect
 *  */

	int pagesize;
        pagesize = sysconf(_SC_PAGE_SIZE);
        //mastercontact=strdup((const char *)argv[2]);
        
	posix_memalign((void **)(&rangebins),sysconf(_SC_PAGE_SIZE),sizeof(int));
        app_setup("rangebins",'i');
/*Logic ends*/
	int i,j;
	int nxy=NX*NY;
	double complex R[NX][NY],Rinv[nxy][nxy];
//	char string_list[2048];
	// CManager cm;
        //EVstone stone;
        EVsource src;
	dfg_get_master_contact_func(&mastercontact[0]);
        EVclient_sources source_capabilities, ctrl_source_capabilities, a_source_capabilities;
	EVclient_sinks sink_capabilities;
	//EVclient_sinks sink_capabilities;
	//EVclient test_client;
	//EVdfg test_dfg;
	
//        attr_list contact_list;
  //      EVstone remote_stone;
	struct timeval start, end, elapsed;
	
	/*if (sscanf(argv[1], "%d:%s", &remote_stone, &string_list[0]) != 2) {

        printf("Bad arguments \"%s\"\n", argv[1]);
        exit(0);
 	   }
	*/
	//int addr;
	//(void) inet_aton(DEST_IP, (struct in_addr *)&addr);
        cm=CManager_create();


	//test_dfg = EVdfg_create(cm);
	//source_join_info src_info;
	//src_info.node_name="c1_ap";
	//src_info.stone_name="c1_app_src";
	//src_info.simple_format_list=inverse_format_list;
	//src_info.node_dfg=test_dfg;
	//src_info.src=&source;
	
//	dfg_source_join(cm,&src_info);
	

	char sourcename[]="src1_";
	char ctrl_src_name[] = "src_ctrl_";
	char ctrl_sink_name[] = "sink_ctrl_";
	char a_sourcename[]="src_a_spy_";
	//char a_sinkname[]="sink_a_spy_cnode";	
	
	//char sinkname[]="sink1_cnode";
//	char nodename[]="cnode";
	char *nodename;
	nodename = strdup((const char *)argv[1]);
	
	strcat(sourcename,nodename);
        strcat(ctrl_src_name,nodename);
	strcat(ctrl_sink_name,nodename);
	strcat(a_sourcename,nodename);

	src = EVcreate_submit_handle(cm,DFG_SOURCE,inverse_format_list);
	src_dum = EVcreate_submit_handle(cm, DFG_SOURCE,ctrl_msg_rec_format_list);
	
	a_source = EVcreate_submit_handle(cm,DFG_SOURCE,mon_app_spy_format_list);
        a_source_capabilities=EVclient_register_source(a_sourcename, a_source);
              //    test_client = EVclient_assoc(cm,nodename,mastercontact,source_capabilities,NULL);
                //  EVclient_ready_wait(test_client);

	source_capabilities = EVclient_register_source(sourcename,src);
	ctrl_source_capabilities = EVclient_register_source(ctrl_src_name, src_dum);
	sink_capabilities = EVclient_register_sink_handler(cm,ctrl_sink_name,ctrl_msg_rec_format_list,(EVSimpleHandlerFunc) ctrl_handler, NULL);
	
	printf("nodename, mastercontact %s %s \n",nodename,mastercontact);
	//test_client = EVclient_assoc(cm,nodename,mastercontact,source_capabilities,sink_capabilities);
	  test_client = EVclient_assoc(cm,nodename,mastercontact,source_capabilities,sink_capabilities);
	test_client = EVclient_assoc(cm,nodename,mastercontact,a_source_capabilities,NULL);
	 test_client = EVclient_assoc(cm,nodename,mastercontact,ctrl_source_capabilities,NULL);

	EVclient_ready_wait(test_client);
	//EVclient_ready_wait(a_test_client);
//	EVdfg_ready_wait(test_dfg);


	
//	if(src_info.src==NULL)
//	printf("\n evsource is null\n");
	
	if(EVclient_source_active(src))
	{
		printf("Submitting record\n");
		inverse inv;
	        gettimeofday(&start,NULL);
//		clock_gettime(CLOCK_REALTIME,&start);
		calc_covariance(&Rinv[0][0],&R[0][0]);


	        for(i=0;i<nxy;++i)
        	{
                	for(j=0;j<nxy;++j)
                	{
	                inv.inv_data[i][j].real=creal(Rinv[i][j]);
        	        inv.inv_data[i][j].imag=cimag(Rinv[i][j]);
        	        }
        	}
	        for(i=0;i<NX;++i)
        	{
                	for(j=0;j<NY;++j)
                	{
	                inv.raw_data[i][j].real=creal(R[i][j]);
        	        inv.raw_data[i][j].imag=cimag(R[i][j]);
                	}
        	}
		
		gettimeofday(&sent,NULL);	
//		clock_gettime(CLOCK_REALTIME,&sent);
		  EVsubmit(src,&inv,NULL);
		
		 gettimeofday(&end,NULL);
//		clock_gettime(CLOCK_REALTIME,&end);
		 elapsed.tv_sec=(end.tv_sec)-(start.tv_sec);
	        elapsed.tv_usec=(end.tv_usec)-(start.tv_usec);
        	printf("\n Cnode data sent at %ld usec\n", sent.tv_usec + (sent.tv_sec * 1000000));
        	printf("\nCovariance Elapsed Time %f usec\n",(((double)elapsed.tv_usec)+((double)elapsed.tv_sec*1000000)));
	         mprotect(rangebins, sizeof(int),4);
	//	EVfree_source(src);
//        	free(rangebins);

		
	}
	
	 /*if (EVdfg_active_sink_count(test_dfg) == 0) {
	    EVdfg_ready_for_shutdown(test_dfg);
    	}*/

	//EVdfg_wait_for_shutdown(test_dfg);
	
	 //EVclient_ready_for_shutdown(test_client);
         //EVclient_wait_for_shutdown(test_client);
	
	
        /*CMlisten(cm);
        stone=EValloc_stone(cm);
	contact_list=create_attr_list();
	contact_list=attr_list_from_string(string_list);
       	EVassoc_bridge_action(cm,stone,contact_list,remote_stone);
        source=EVcreate_submit_handle(cm,stone,inverse_format_list);
	inverse inv;

	gettimeofday(&start,NULL);
		
	
	calc_covariance(&Rinv,&R);


	for(i=0;i<nxy;++i)
	{
		for(j=0;j<nxy;++j)
		{
		inv.inv_data[i][j].real=creal(Rinv[i][j]);
		inv.inv_data[i][j].imag=cimag(Rinv[i][j]);
		}
	}
	for(i=0;i<NX;++i)
	{
		for(j=0;j<NY;++j)
		{
		inv.raw_data[i][j].real=creal(R[i][j]);
		inv.raw_data[i][j].imag=cimag(R[i][j]);
		}
	}
	
	gettimeofday(&sent,NULL);
	EVsubmit(source,&inv,NULL);
	gettimeofday(&end,NULL);
	elapsed.tv_sec=(end.tv_sec)-(start.tv_sec);
	elapsed.tv_usec=(end.tv_usec)-(start.tv_usec);
	printf("\n Cnode data sent at %ld usec\n", sent.tv_usec + (sent.tv_sec * 1000000));
	printf("\nCovariance Elapsed Time %f usec\n",(((double)elapsed.tv_usec)+((double)elapsed.tv_sec*1000000)));
	 mprotect(rangebinsptr, sizeof(int),4);
	free(rangebinsptr);*/
	
}	

