#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include "../../include/evpath.h"
#include "../../include/atl.h"

// the identifier of the monitoring event
enum cw_MON_EVENT {
	cw_MON_EVENT_CPU,
	cw_MON_EVENT_MEM,
	cw_MON_EVENT_NET,
	cw_MON_EVENT_NVML,
	cw_MON_EVENT_LYNX,
	cw_MON_EVENT_ADBF
};

//receive events from CW
struct cw_mon_event {
	enum cw_MON_EVENT id;
	// the pointer to the function that will be handling 
	// the monitoring events incoming from CW
	handler_t handler;
	// parameter for the EVPath handler
	void *client_data;
	// parameter for EVPath handler
	attr_list attrs;
	// the duration of the monitoring; 
	// 0 value - monitor forever; >0 value - monitor
	//for the specified duration 
	int sec;
	// this is for future implementation, 
	// e.g. specify the window for 
	// reporting the aggregated values
	int window_in_sec;
};

//receive events from control parent agent
enum ctrl_EVENT_ID {
	ctrl_START_APP,
	ctrl_STOP_APP,
	ctrl_DESCOPE,
	ctrl_OVERSAMPLE,
//	ctrl_RECONFIGURE - only root can reconfigure
};

struct ctrl_event {
	enum ctrl_EVENT_ID ctrl_id;
	

		
};
