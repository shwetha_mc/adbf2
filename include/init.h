#ifndef HEADER_FILE
#define HEADER_FILE
#include <complex.h>
#define NX 16
#define NY 16

double pseudorandn();
void initspace(double* x, double* y);
void steering_vector(double complex* sv, double x[], double y[], double az, double el);
void signalcomp(double complex* sig,  double amp, double az, double el);

#endif
